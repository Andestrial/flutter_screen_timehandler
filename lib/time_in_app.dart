import 'package:flutter/material.dart';
import 'package:flutter_screen_timehandler/flutter_screen_timehandler_platform_interface.dart';

class TimeInApp extends StatefulWidget {
  const TimeInApp({Key? key}) : super(key: key);

  @override
  State<TimeInApp> createState() => _TimeInAppState();
}

class _TimeInAppState extends State<TimeInApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Time In App Info"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ...FlutterScreenTimehandlerPlatform.listOfScreensModels
                .map((e) => ListTile(
                      title: Text(e.name),
                      trailing: Text(e.time.toString()),
                    )),
            ListTile(
              title: Text("All time in App"),
              trailing: Text(FlutterScreenTimehandlerPlatform.millisTiTime(
                  FlutterScreenTimehandlerPlatform.allTimeInApp)),
            )
          ],
        ),
      ),
    );
  }
}

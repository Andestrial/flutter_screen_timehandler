import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_screen_timehandler_platform_interface.dart';

/// An implementation of [FlutterScreenTimehandlerPlatform] that uses method channels.
class MethodChannelFlutterScreenTimehandler extends FlutterScreenTimehandlerPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_screen_timehandler');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}

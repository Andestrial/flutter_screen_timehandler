
import 'flutter_screen_timehandler_platform_interface.dart';

class FlutterScreenTimehandler {
  Future<String?> getPlatformVersion() {
    return FlutterScreenTimehandlerPlatform.instance.getPlatformVersion();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_screen_timehandler/flutter_screen_timehandler_platform_interface.dart';
import 'package:flutter_screen_timehandler/time_in_app.dart';
import 'package:flutter_screen_timehandler/widget_extension.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return StatefulWidgetWrapper(
      runtimeTypeWidget: widget.runtimeType.toString(),
      child: const MaterialApp(
        home: FirstScreen()
      ),
    );
  }
}

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {


  var counter = 0;

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return StatefulWidgetWrapper(
      runtimeTypeWidget: widget.runtimeType.toString(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              MaterialButton(
                child: Text("To Next Screen"),
                  color: Colors.cyan,
                  onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (_) => const SecondScreen()));
              })
            ],
          ),
        ),
      ),
    );
  }
}


class SecondScreen extends StatefulWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StatefulWidgetWrapper(
      runtimeTypeWidget: widget.runtimeType.toString(),
      child:  Scaffold(
          appBar: AppBar(
            title: const Text('Plugin example app'),
          ),
          body: Center(
            child: Column(
              children: [
                ...FlutterScreenTimehandlerPlatform.listOfScreensModels.map((e) => Text("${e.name} ${e.time}")),
                Text(FlutterScreenTimehandlerPlatform.allTimeInApp.toString()),
                MaterialButton(
                    color: Colors.cyan,
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => const TimeInApp()));
                    },
                    child: const Text("To Next Screen"))
              ],
            ),
          ),
        ),
    );
  }
}


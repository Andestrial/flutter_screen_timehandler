import 'dart:async';

import 'package:flutter_screen_timehandler/time_traking_model.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_screen_timehandler_method_channel.dart';

abstract class FlutterScreenTimehandlerPlatform extends PlatformInterface {
  /// Constructs a FlutterScreenTimehandlerPlatform.
  FlutterScreenTimehandlerPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterScreenTimehandlerPlatform _instance =
      MethodChannelFlutterScreenTimehandler();

  /// The default instance of [FlutterScreenTimehandlerPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterScreenTimehandler].
  static FlutterScreenTimehandlerPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterScreenTimehandlerPlatform] when
  /// they register themselves.
  static set instance(FlutterScreenTimehandlerPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  static int timeOnScreen = 0;

  static late Timer _timer;

  static num allTimeInApp = 0;

  static void startTimer() {
    _timer = Timer.periodic(const Duration(milliseconds: 1), (timer) {
      timeOnScreen = _timer.tick;
    });
  }

  static Map<String, dynamic> listOfScreens = {};

  static List<TimeTracingModel> listOfScreensModels = [];

  static void cancelTimer(String widgetName) async {
    if (listOfScreens.containsKey(widgetName)) {
      listOfScreens.update(widgetName, (value) => value += timeOnScreen);
      var oldElement = listOfScreensModels
          .where((element) => element.name == widgetName)
          .first;
      var index = listOfScreensModels.indexOf(listOfScreensModels
          .where((element) => element.name == widgetName)
          .first);
      listOfScreensModels.removeAt(index);
      listOfScreensModels.insert(index,
          TimeTracingModel(oldElement.name, timeOnScreen += oldElement.time));
    } else {
      listOfScreens.addAll({widgetName: timeOnScreen});

      listOfScreensModels.add(TimeTracingModel(widgetName, timeOnScreen));
    }
    timeOnScreen = 0;
    allTimeInApp = 0;
    for (var element in listOfScreens.values) {
      allTimeInApp += element;
    }

    _timer.cancel();
  }

  static String millisTiTime(num value) {
    var SS = ((value / 1000) % 60).round();
    var MM = ((value / (1000 * 60)) % 60);
    var HH = ((value / (1000 * 60 * 60)) % 24);

    var SSstring = SS < 10 ? "0${SS}" : "$SS";
    var MMstring = MM < 10
        ? "0${MM.toString().substring(0, 1)}"
        : "${MM.toString().substring(0, 1)}";
    var HHstring = HH < 10
        ? "0${HH.toString().substring(0, 1)}"
        : "${HH.toString().substring(0, 1)}";
    return "$HHstring:$MMstring:$SSstring";
  }

}



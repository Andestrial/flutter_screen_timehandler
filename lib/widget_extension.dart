import 'package:flutter/widgets.dart';
import 'package:flutter_screen_timehandler/flutter_screen_timehandler_platform_interface.dart';
import 'package:focus_detector/focus_detector.dart';

class StatefulWidgetWrapper extends StatefulWidget {
  final Widget child;
  final String runtimeTypeWidget;

  const StatefulWidgetWrapper({
    Key? key,
    required this.child,
    required this.runtimeTypeWidget,
  }) : super(key: key);

  @override
  State<StatefulWidgetWrapper> createState() => _StatefulWidgetWrapper();
}

class _StatefulWidgetWrapper extends State<StatefulWidgetWrapper> {
  @override
  Widget build(BuildContext context) {
    return FocusDetector(
        onFocusGained: () {
          FlutterScreenTimehandlerPlatform.startTimer();
        },
        onFocusLost: () async {
          FlutterScreenTimehandlerPlatform.cancelTimer(widget.runtimeTypeWidget);
        },
        child: widget.child);
  }
}

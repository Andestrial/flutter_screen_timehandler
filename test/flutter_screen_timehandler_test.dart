import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_screen_timehandler/flutter_screen_timehandler.dart';
import 'package:flutter_screen_timehandler/flutter_screen_timehandler_platform_interface.dart';
import 'package:flutter_screen_timehandler/flutter_screen_timehandler_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterScreenTimehandlerPlatform
    with MockPlatformInterfaceMixin
    implements FlutterScreenTimehandlerPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final FlutterScreenTimehandlerPlatform initialPlatform = FlutterScreenTimehandlerPlatform.instance;

  test('$MethodChannelFlutterScreenTimehandler is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterScreenTimehandler>());
  });

  test('getPlatformVersion', () async {
    FlutterScreenTimehandler flutterScreenTimehandlerPlugin = FlutterScreenTimehandler();
    MockFlutterScreenTimehandlerPlatform fakePlatform = MockFlutterScreenTimehandlerPlatform();
    FlutterScreenTimehandlerPlatform.instance = fakePlatform;

    expect(await flutterScreenTimehandlerPlugin.getPlatformVersion(), '42');
  });
}
